package com.epam.edu.online.pingpong;

class Ping extends Thread {
    private Table table;
    private String ping = "";

    public Ping(Table t){
        table = t;
    }
    public void run() {

        for (int i = 0; i < 10; i++) {
            ping = table.getPing();
            System.out.println(ping + " - " + Thread.currentThread().getName());
        }
    }
}


class Pong extends Thread {
    private Table table;
    private String pong = "";

    public Pong(Table t){
        table = t;
    }
    public void run() {
        for (int i = 0; i < 10; i++) {
            pong = table.getPong();
            System.out.println(pong + " - " + Thread.currentThread().getName());   }
    }
}

class Table extends Thread {
    private boolean pinged = false;

    public synchronized String getPong() {
        while (pinged == false) {
            try {
                wait();
            }
            catch (InterruptedException e){

            }
        }
        pinged = false;
        notifyAll();
        return "pong";
    }

    public synchronized String getPing() {
        while (pinged) {
            try {
                wait();
            } catch (InterruptedException e) {

            }
        }
        pinged = true;
        notifyAll();

        return "ping";
    }
}


public class PingPong {
    public static void main(String[] args) {
        Table table = new Table();
        Pong pong = new Pong(table);
        Ping ping = new Ping(table);
        pong.start();
        ping.start();
    }

}

package com.epam.edu.online.fibonacci;

public class FibonacciPrint implements Runnable {
    private Fibonacci fibonacci;

    FibonacciPrint(int size) {
        fibonacci = new Fibonacci(size);
    }

    @Override
    public void run() {
        fibonacci.printSequenceFibonacci();
    }

}

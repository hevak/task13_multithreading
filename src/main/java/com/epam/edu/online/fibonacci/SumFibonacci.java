package com.epam.edu.online.fibonacci;

import java.util.concurrent.Callable;

public class SumFibonacci implements Callable<Long> {
    private int size;

    public SumFibonacci(int size) {
        this.size = size;
    }

    @Override
    public Long call() throws Exception {
        return new Fibonacci(size)
                .getFibonacciNumbers()
                .stream()
                .reduce((long1, long2) -> long1 + long2)
                .get();
    }
}

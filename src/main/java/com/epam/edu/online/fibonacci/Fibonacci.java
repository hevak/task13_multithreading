package com.epam.edu.online.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {
    private static final Logger log = LogManager.getLogger(FibonacciPrint.class);

    private long value;
    private long secondValue;
    private long firstValue;
    private long size;

    Fibonacci(int size) {
        this.firstValue = 0;
        this.secondValue = 1;
        this.value = 0;
        this.size = size;
    }
    public void printSequenceFibonacci() {
        long startTime = System.currentTimeMillis();
        getFibonacciNumbers().forEach(log::info);
        printTime(startTime);
    }

    public List<Long> getFibonacciNumbers() {
        int counter = 0;
        List<Long> fibonacciNumber = new ArrayList();
        while (counter++ < size) {
            fibonacciNumber.add(value);
            firstValue = value + secondValue;
            value = secondValue;
            secondValue = firstValue;
        }
        return fibonacciNumber;
    }

    private void printTime(long startTime) {
        log.info((System.currentTimeMillis() - startTime) + " millis");
    }
}

package com.epam.edu.online.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {
    private static final Logger log = LogManager.getLogger(Application.class);
    private static ResourceBundle app = ResourceBundle.getBundle("app");
    private static int size;

    static {
        try {
            size = Integer.parseInt(app.getString("size"));
        } catch (NumberFormatException e) {
            size = 10;
        }
    }

    public static void main(String[] args) {
        Thread currentThread = Thread.currentThread();
        log.info("Main thread name : " + currentThread.getName());

        // start with runnable
        new Thread(new FibonacciPrint(size), "Fibonacci").start();
        // start with executor
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(new FibonacciPrint(size));
        service.execute(new FibonacciPrint(size));
        // not execute, because fixed 2 threads
        service.execute(new FibonacciPrint(size));

        // Sum of numbers Fibonacci
        try {
            log.info("Sum of " + size + " numbers Fibonacci - "
                    + CompletableFuture.supplyAsync(() -> new SumFibonacci(size)).get().call());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

package com.epam.edu.online.randomamount;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class Application {
    private static final Logger log = LogManager.getLogger(Application.class);
    private static ResourceBundle app = ResourceBundle.getBundle("app");
    private static int iterations;
    static {
        try {
            iterations = Integer.parseInt(app.getString("iterations"));
        } catch (NumberFormatException e) {
            iterations = 10;
        }
    }


    public static void main(String[] args) {
        new Thread(() -> {
            for (int i = 0; i < iterations; i++) {
                try {
                    int timeout = new Random().nextInt(10);
                    log.info(i + " iterations - " + timeout + "sec. timeout");
                    TimeUnit.SECONDS.sleep(timeout);
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }
            }
        }).run();





    }

}

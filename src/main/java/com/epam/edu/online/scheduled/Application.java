package com.epam.edu.online.scheduled;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.concurrent.*;

public class Application {
    private static final Logger log = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Runnable task = () -> log.info("message");

        log.info("The time is : " + new Date());

        ScheduledFuture<?> result = executor.scheduleWithFixedDelay(task, 2, 3, TimeUnit.SECONDS);
        try {
            log.info(result.get());
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        } catch (ExecutionException e) {
            log.error(e.getMessage());
        }

        executor.shutdown();




    }
}

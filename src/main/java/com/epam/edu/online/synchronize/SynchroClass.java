package com.epam.edu.online.synchronize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchroClass {
    private static final Logger log = LogManager.getLogger(SynchroClass.class);

    static int field = 0;
    private static Lock lock = new ReentrantLock();
//    private static Object sync1 = new Object();
//    private static Object sync2 = new Object();
//    private static Object sync3 = new Object();

    public static void showInfo() {
        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(100);
                lock.lock();
                try {
                    log.info(field);
                } finally {
                    lock.unlock();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void increment() {
        for (int i = 0; i < 100; i++) {
            lock.lock();
            try {
                field++;
            } finally {
                lock.unlock();
            }
        }
    }

    public static void decrement() {
        for (int i = 0; i < 50; i++) {
            lock.lock();
            try {
                field--;
            } finally {
                lock.unlock();
            }
        }
    }
}

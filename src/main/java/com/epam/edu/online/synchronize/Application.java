package com.epam.edu.online.synchronize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static final Logger log = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        long startTime = System.nanoTime();

        Thread thread1 = new Thread(SynchroClass::showInfo);
        Thread thread2 = new Thread(SynchroClass::increment);
        Thread thread3 = new Thread(SynchroClass::decrement);

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("Total time  - " + (double) (System.nanoTime()-startTime) / 1_000_000_000 + "sec.");

    }
}
